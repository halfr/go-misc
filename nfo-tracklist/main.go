// nfo-tracklist: helper for generating NFO files music files
//
// stdin input format:
//
//     filename1
//     number of seconds as float (e.g. 34.2)
//     ...
//     filename2
//     number of seconds as float (e.g. 34.2)
//
// Sample usage:
//
//     (for i in *.mp3;
//       (
//          echo $i | sed -r 's/....-..-..-(.*)\.mp3/\1/'
//          ffprobe -show_streams -select_streams a -print_format json $i 2>/dev/null | jq '.streams[0].duration'
//       )
//     ) | nfo-tracklist
//
// Sample outout:
//
//     1. Une histoire du Cinéma français - 1ère partie                      [46:17]
//     2. Une histoire du Cinéma français - 2ème partie                      [42:01]
//     3. La découverte du Temps - 1                                         [42:39]
//     4. La découverte du Temps - 2                                         [30:38]
//     5. Les Faux-Monnayeurs sous la Révolution                             [45:54]
//     6. Charles V - Episode 1                                              [44:07]
package main

import "strings"
import "fmt"
import "bufio"
import "os"
import "golang.org/x/exp/utf8string"

const lineLength = 80

func pad(i *utf8string.String, add int) string {
	var out string
	maxStrLen := lineLength - add
	if i.RuneCount() > maxStrLen {
		out = i.Slice(0, maxStrLen-1)
		if out[len(out)-1] == ' ' {
			out = out[0 : len(out)-1]
		}
		out = out + "…"
	} else {
		out = i.String()
	}
	return out + strings.Repeat(" ", maxStrLen-utf8string.NewString(out).RuneCount())
}

func fmtTime(seconds float64) string {
	hours := seconds / 3600
	inputi := int(seconds) % 3600
	minutes := inputi / 60
	secondsi := inputi % 60

	if hours >= 1 {
		return fmt.Sprintf("%02d:%02d:%02d", int(hours), minutes, secondsi)
	} else {
		return fmt.Sprintf("%02d:%02d", minutes, secondsi)
	}
}

func main() {
	totalTime := 0.0
	i := 1
	for {
		if i >= 126 {
			break
		}
		reader := bufio.NewReader(os.Stdin)
		bname, _, _ := reader.ReadLine()
		name := utf8string.NewString(string(bname))

		var inputf float64
		fmt.Scanf(`"%f"\n`, &inputf)
		totalTime = totalTime + inputf

		counter := fmt.Sprintf("% 4d.", i)
		time := "[" + fmtTime(inputf) + "]"

		fmt.Printf("%s %s %s\n", counter, pad(name, 2+len(counter)+len(time)), time)

		i = i + 1
	}

	fmt.Printf("\nPlaying Time.........: %s\n", fmtTime(totalTime))
}
